apt-get install -y git-core git
apt-get install -y sudo

echo Install Nodejs
curl -sL https://deb.nodesource.com/setup_6.x | bash -
apt-get install -y nodejs

echo Install n
npm install -g n
n lts

echo Install curl
apt-get install curl -y

echo Install wget
apt-get install wget -y

echo Download bashrc
wget https://bitbucket.org/maxoux/install_script/raw/master/.bashrc -O ~/.bashrc
wget https://bitbucket.org/maxoux/install_script/raw/master/.git-prompt.sh -O ~/.git-prompt.sh

echo Download tools
apt-get install htop -y
apt-get install numlockx -y

echo Install bash-completion
apt-get install -y bash-completion

echo Reinit shell
source ~/.bashrc
